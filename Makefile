CC=gcc
CCFLAGS=-W -Wall -Wextra -ansi -pedantic-errors -Werror
LIBS=-lm

all: the1

debug: CCFLAGS += -D__DEBUG__ -g
debug: the1

release: CCFLAGS += -O2
release: the1

native: CCFLAGS += -mtune=native
native: release

the1:
	$(CC) -o build/the1 the1.c $(CCFLAGS) $(LIBS)

