#include <stdio.h>
#include <stdlib.h>
#include <string.h>	/* strncmp */
#include <time.h>
#include <float.h> /* DBL_MAX */
#include <math.h>

#ifdef __GNUC__
#ifndef __DEBUG__
	#pragma GCC optimize("O2")
#endif
#endif /* TODO: check for other compilers */

#define TOKENSTACK_SIZE	(200)
#define TOKENQUEUE_SIZE	(200)
#define COUNT_FUNCTIONS (5)
#define COUNT_PDF_MAX (100)
#define NUM_VARIABLES ('Z'-'A'+1)	/* damn off-by-one errors */

#define UNUSED(x) (void)(x)

typedef double fp_t; /* single precision */
#define FP_FMTSTRING "%lf"

/*************************************/
/* Runtime error handling / abort    */
/*************************************/

void fail(const char *msg) {
	#ifdef __DEBUG__
	fprintf(stderr, "Failed: %s\n", msg);
	exit(-1);
	#else
	UNUSED(msg);
	#endif
}

/*************************************/
/* Enums                             */
/*************************************/

typedef enum {
	TokenInvalid = 0,

	TokenOperator,
	TokenFunction,

	TokenLParen, TokenRParen,

	TokenVariable,
	TokenConstant
} TokenType;

typedef enum {
	OpInvalid=0, OpAdd, OpSub, OpMul, OpDiv, OpPow
} OperatorType;

typedef enum {
	FnInvalid=0, FnNeg, FnSin, FnCos, FnSqrt, FnLn
} FunctionType;
/* ensure same order between these two */
const char * const FunctionNames[] = {
	0, "~", "sin", "cos", "sqrt", "ln"
};

typedef enum {
	AssocLeft, AssocRight
} OperatorAssoc;

/**************************************/
/* Token Object                       */
/**************************************/

typedef struct {
	TokenType type;
	union {
		OperatorType operator;
		FunctionType function;
		char variable;
		fp_t constant;
	} value;
} token_t;

token_t token_new_operator(OperatorType op) {
	token_t tmp;
	tmp.type = TokenOperator;
	tmp.value.operator = op;
	return tmp;
}

token_t token_new_function(FunctionType fn) {
	token_t tmp;
	tmp.type = TokenFunction;
	tmp.value.function = fn;
	return tmp;
}

token_t token_new_variable(char variable) {
	token_t tmp;
	tmp.type = TokenVariable;
	tmp.value.variable = variable;
	return tmp;
}

token_t token_new_constant(fp_t d) {
	token_t tmp;
	tmp.type = TokenConstant;
	tmp.value.constant = d;
	return tmp;
}

token_t token_new_lparen() {
	token_t tmp;
	tmp.type = TokenLParen;
	return tmp;
}

token_t token_new_rparen() {
	token_t tmp;
	tmp.type = TokenRParen;
	return tmp;
}

int token_isinvalid(token_t t) {
	return (t.type == TokenInvalid);
}

int token_isoperator(token_t t) {
	return (t.type == TokenOperator);
}

int token_isfunction(token_t t) {
	return (t.type == TokenFunction);
}

int token_islparen(token_t t) {
	return (t.type == TokenLParen);
}

int token_isrparen(token_t t) {
	return (t.type == TokenRParen);
}

int token_isparen(token_t t) {
	return (t.type == TokenLParen) || (t.type == TokenRParen);
}

int token_isvariable(token_t t) {
	return (t.type == TokenVariable);
}

int token_isconstant(token_t t) {
	return (t.type == TokenConstant);
}

OperatorType token_get_operator(token_t t) {
	return t.value.operator;
}

FunctionType token_get_function(token_t t) {
	return t.value.function;
}

char token_get_variable(token_t t) {
	return t.value.variable;
}

fp_t token_get_constant(token_t t) {
	return t.value.constant;
}

void token_dump(token_t t) {
	char c='?';
	switch(t.type) {
		case TokenInvalid:
			printf("Invalid\n"); 
			break;
		case TokenConstant:
			printf("Constant|value:%f\n", token_get_constant(t));
			break;
		case TokenVariable:
			printf("Variable|value:%c\n", token_get_variable(t));
			break;
		case TokenOperator:
			switch(token_get_operator(t)) {
				case OpInvalid: c='?'; break;
				case OpAdd: c='+'; break;
				case OpSub: c='-'; break;
				case OpMul: c='*'; break;
				case OpDiv: c='/'; break;
				case OpPow: c='^'; break;
			}
			printf("Operator|type:%c\n", c);
			break;
		case TokenFunction:
			printf("Function|name:%s\n", FunctionNames[token_get_function(t)]);
			break;
		case TokenLParen:
			printf("LParen\n");
			break;
		case TokenRParen:
			printf("RParen\n");
			break;
		default:
			break;
	}
}

/* Token object class helper functions   */
int get_precedence(token_t t) {
	if (token_isoperator(t)) {
		switch(token_get_operator(t)) {
			case OpAdd:
			case OpSub:
				return 1;
			case OpMul:
			case OpDiv:
				return 2;
			case OpPow:
				return 3;
			default:
				return -1;
				break;
		}
	} else if (token_isfunction(t)) {
		return 4;
	} else {
		return -1;	/* error: not an operator or a function */
	}
}

OperatorAssoc get_assoc(token_t t) {
	if (token_isoperator(t)) {
		switch(token_get_operator(t)) {
			case OpAdd:
			case OpSub:
			case OpMul:
			case OpDiv:
				return AssocLeft;
			case OpPow:
				return AssocRight;
			default:
				return -1;
				break;
		}
	} else {
		return -1;	/* error: not an operator or a function */
	}
}

OperatorType get_operator(const char *s) {
	switch(s[0]) {
		case '+': return OpAdd;
		case '-': return OpSub;
		case '*': return OpMul;
		case '/': return OpDiv;
		case '^': return OpPow;
		default:
			return OpInvalid;
			break;
	}
}

FunctionType get_function(const char *s) {
	int i;

	/* 1 invalid, 5 real operators */
	for (i=1; i<(COUNT_FUNCTIONS+1); ++i) {
		if (strncmp(FunctionNames[i], s, strlen(FunctionNames[i])) == 0)
			return i;	/* corresponds to enum */
	}

	return FnInvalid;
}

int is_space(const char *s) {
	char c = s[0];
	return (c == ' ');
}

int is_numeric(const char *s) {
	char c = s[0];
	/* all constants will be of the form X or X.YZ as unsigned fp numbers */	
	return (c == '.') || ((c >= '0') && (c <= '9'));
}

int is_variable(const char *s) {
	char c = s[0];
	/* variables are A..Z */
	return (c >= 'A') && (c <= 'Z');
}

int is_operator(const char *s) {
	char c = s[0];
	return (c == '+') || (c == '-') || (c == '*') || (c == '/') || (c == '^'); 
}

int is_lparen(const char *s) {
	return *s == '(';
}

int is_rparen(const char *s) {
	return *s == ')';
}

int is_paren(const char *s) {
	return is_lparen(s) || is_rparen(s);
}

int is_function(const char *s) {
	/* if not a number, variable or operator, then surely it is a function */
	/* this is guaranteed as there will be no erroneous inputs */
	return (!is_numeric(s)) 
		&& (!is_variable(s)) 
		&& (!is_operator(s))
		&& (!is_paren(s));
}


/**************************************/
/* END Token Object                   */
/**************************************/

/****************************/
/* Variable PDF object      */
/****************************/

typedef struct {
	char name;
	fp_t lower_limit, upper_limit;
	fp_t cdf[COUNT_PDF_MAX];
	size_t count_intervals;
} variable_t;

/* Variable PDF description is of standard form */
/* A 1 10 0.01 ... 0.45 */
variable_t variable_parse_str(const char *str, size_t count_intervals) {
	int scanf_length;
	size_t i;
	fp_t cdf = 0.0, pdf;
	variable_t rv;

	if (!is_variable(str)) {
		fail("parse variable: not a valid variable name");
	}

	if (!sscanf(str, "%c "FP_FMTSTRING" "FP_FMTSTRING" %n", &rv.name, &rv.lower_limit, &rv.upper_limit, &scanf_length)) {
		fail("parse variable limits failed");
	}

	if (!(rv.lower_limit < rv.upper_limit)) {
		fail("parse variable limits invalid");
	}

	str += scanf_length;

	for (i=0; i<count_intervals; ++i) {
		sscanf(str, ""FP_FMTSTRING" %n", &pdf, &scanf_length);
		if (pdf<0.0 || pdf>1.0)
			fail("probability out of range [0,1]");

		str += scanf_length;
		cdf += pdf;
		if (cdf > 1.0)
			fail("cumulative probability reached over 1.0");
		
		rv.cdf[i] = cdf;
	}

	rv.count_intervals = count_intervals;
	return rv;
}

void variable_dump(variable_t *var) {
	size_t i;

	printf("| Variable name: %c, lower limit: %g, upper limit: %g\n| CDF: ", 
		var->name, var->lower_limit, var->upper_limit);
	for (i=0; i<var->count_intervals; ++i)
		printf("%.6g ", var->cdf[i]);
	printf("\n");
}

/****************************/
/* END Variable PDF object  */
/****************************/

/***************************************/
/* Token Stack                         */
/***************************************/

typedef struct {
	token_t data[TOKENSTACK_SIZE];
	int index;
} token_stack_t;

void stack_init(token_stack_t *ts) {
	/* no heap allocation */
	ts->index = -1;
}

void stack_deinit(token_stack_t *ts) {
	UNUSED(ts);
	/* no heap deallocation */
}

int stack_isempty(token_stack_t *ts) {
	return (ts->index == -1);
}

int stack_isfull(token_stack_t *ts) {
	return (ts->index == TOKENSTACK_SIZE-1);
}

token_t stack_top(token_stack_t *ts) {
	return ts->data[ts->index];
}

void stack_pop(token_stack_t *ts) {
	if (!stack_isempty(ts)) {
		--ts->index;
	} else {
		fail("stack underflow");
	}
}

void stack_push(token_stack_t *ts, token_t t) {
	if (!stack_isfull(ts)) {
		ts->data[++ts->index] = t;
	} else {
		fail("stack overflow");
	}
}

/***************************************/
/* END Token Stack                     */
/***************************************/

/***************************************/
/* Token Queue                         */
/***************************************/

typedef struct {
	token_t data[TOKENQUEUE_SIZE];
	size_t tail;
} token_queue_t;

void queue_init(token_queue_t *tq) {
	/* no heap allocation */
	tq->tail = 0;
}

void queue_deinit(token_queue_t *tq) {
	UNUSED(tq);
	/* no heap deallocation */
}

int queue_isfull(token_queue_t *tq) {
	return (tq->tail == TOKENQUEUE_SIZE);
}

void queue_push(token_queue_t *tq, token_t t) {
	if (!queue_isfull(tq)) {
		tq->data[tq->tail++] = t;
	} else {
		fail("queue overflow");
	}
}

/***************************************/
/* END Token Queue                     */
/***************************************/

/****************************/
/* utilities                */
/****************************/

void util_init() {
	srand(time(0));
}

fp_t util_randomd() {
	return ((fp_t) rand()) / RAND_MAX;
}

fp_t util_lerp(fp_t a, fp_t b, fp_t t) {
	return a + t * (b-a);
}

size_t util_lower_bound(fp_t arr[], size_t length, fp_t val) {
	size_t first = 0, count = length;
	while (count > 0) {
		size_t it = first, step = count/2; 
		it += step;
		if (arr[it] < val) {
			first = ++it;
			count -= step + 1;
		}
		else count = step;
	}
	return first;
}

fp_t util_random_sample(variable_t *var) {
	size_t lower_index = 0;
	fp_t t, x = util_randomd(), d = (var->upper_limit-var->lower_limit)/var->count_intervals;
	fp_t x1, x2, a, b;

	lower_index = util_lower_bound(var->cdf, var->count_intervals, x);

	if (lower_index == 0)
		x1 = 0;
	else
		x1 = var->cdf[lower_index-1];
	x2 = var->cdf[lower_index];

	a = var->lower_limit + (lower_index) * d;
	b = var->lower_limit + (lower_index+1) * d;

	t = (x-x1) / (x2-x1);

	return util_lerp(a, b, t);
}

void util_random_sample_all(fp_t output[], variable_t variables[], size_t count) {
	size_t i;
	for (i = 0; i < count; ++i) {
		if (variables[i].name) {
			output[i] = util_random_sample(&variables[i]);
		} else {
			output[i] = 0.0;
		}
	}
}

size_t util_get_bin(fp_t lower_limit, fp_t upper_limit, unsigned int num_intervals, fp_t value) {
	fp_t d = (upper_limit-lower_limit)/num_intervals;
	if (d > 1e-6)
		return (size_t)((value-lower_limit) / d);
	else
		return 0;
}

size_t util_var_index(char var_name) {
	return (size_t) (var_name - 'A');
}

/* modified from: https://github.com/ivanrad/getline/blob/master/getline.c */
/* note: bugfix on realloc current_pos update */
int util_getline(char **line, size_t *n, FILE *stream) {
	size_t current_pos, new_line_len;
	char *new_line;

	if (line == NULL || n == NULL || stream == NULL) {
		return -1;
	}

	if (*line == NULL) {
		*n = 128;
		if ((*line = (char *) malloc(sizeof(char) * (*n))) == NULL) {
			return -2;
		}
	}

	for (current_pos = 0; ; ++current_pos) {
		int c = getc(stream);
		if (c == EOF)
			break;

		if ((*n - current_pos) < 2) {
			new_line_len = (*n) * 2;

			if ((new_line = (char *) realloc(*line, new_line_len)) == NULL) {
				return -2;
			}
			*line = new_line;
			*n = new_line_len;
		}

		if (c == '\n') /* Windows CRLF convention handled by stdlib */
			break;
		/* do not include the newline, so reverse order */
		(*line)[current_pos] = (char) c;
	}

	(*line)[current_pos] = '\0';
	return (int) current_pos;
}

/****************************/
/* END utilities            */
/****************************/

/****************************/
/* parser function          */
/****************************/

void parse_expression(const char *expr, token_queue_t *queue) {
	const char *s;
	fp_t d; int length;
	token_t new_token;

	token_stack_t stack; 
	stack_init(&stack);

	s = expr;

	while (*s) {
		if (is_space(s)) {
			++s;
		} 
		else if (is_numeric(s)) {
			/* scan the number and advance pointer */
			if (!sscanf(s, ""FP_FMTSTRING"%n", &d, &length)) {
				fail("sscanf failed in numeric constant acquisition");
			}
			s += length; /* advance by the number of letters read */

			new_token = token_new_constant(d);

			/* push constant/variable directly to output */
			queue_push(queue, new_token);

		} 
		else if (is_variable(s)) {
			/* get the variable */
			new_token = token_new_variable(s[0]);
			queue_push(queue, new_token);
			++s; /* advance by one letter */
		} 
		else if (is_operator(s)) {
			new_token = token_new_operator(get_operator(s));
			
			/* if left assoc, pop until (empty or top is paren or top is lower prec) */
			if (get_assoc(new_token) == AssocLeft) {
				while (!stack_isempty(&stack)
					&& !token_isparen(stack_top(&stack))
					&& !(get_precedence(new_token) > get_precedence(stack_top(&stack))))
				{
					queue_push(queue, stack_top(&stack));
					stack_pop(&stack);
				}
			} 
			else if (get_assoc(new_token) == AssocRight) { /* if right assoc, pop until (empty or top is paren or top lower|equal prec) */
				while (!stack_isempty(&stack)
					&& !token_isparen(stack_top(&stack))
					&& !(get_precedence(new_token) >= get_precedence(stack_top(&stack))))
				{
					queue_push(queue, stack_top(&stack));
					stack_pop(&stack);
				}
			} 
			else {
				fail("get_assoc says not a valid operator, check is_operator call");
			}
			/* lastly, push it onto the stack */
			stack_push(&stack, new_token);
			++s; /* advance by operator length */
		} 
		else if (is_function(s)) {
			new_token = token_new_function(get_function(s));
			s += strlen(FunctionNames[token_get_function(new_token)]);
			stack_push(&stack, new_token);
		} 
		else if (is_lparen(s)) {
			/* push lparen directly to the stack */
			new_token = token_new_lparen();
			stack_push(&stack, new_token);
			++s; /* advance by one letter */
		} 
		else if (is_rparen(s)) {
			/* push to output until lparen */
			while (!(token_islparen(stack_top(&stack))))
			{
				queue_push(queue, stack_top(&stack));
				stack_pop(&stack);
				if (stack_isempty(&stack))
					fail("unbalanced expr: extra rparen");
			}
			/* discard matching opening parenthesis */
			stack_pop(&stack);
			/* if top is a function token, forward to output */
			if (token_isfunction(stack_top(&stack))) {
				queue_push(queue, stack_top(&stack));
				stack_pop(&stack);
			}
			++s; /* advance by one letter */
		}
	}

	while (!stack_isempty(&stack)) {
		if (token_isparen(stack_top(&stack)))
			fail("unbalanced expr: extra lparen");
		queue_push(queue, stack_top(&stack));
		stack_pop(&stack);
	}
}

/****************************/
/* END parser function      */
/****************************/

/****************************/
/* evaluator function       */
/****************************/

fp_t evaluate_expression(token_queue_t *postfix_expression, fp_t sampled_variables[]) {
	size_t i, stack_index = 0;
	fp_t op1, op2, val_stack[TOKENSTACK_SIZE]={0};

	for (i = 0; i < postfix_expression->tail; ++i) {
		token_t cur_token = postfix_expression->data[i];
		if (token_isconstant(cur_token)) {
			val_stack[stack_index++] = token_get_constant(cur_token);
		} else if (token_isvariable(cur_token)) {
			val_stack[stack_index++] = sampled_variables[
				util_var_index(token_get_variable(cur_token))
				];
		} else if (token_isfunction(cur_token)) {
			/* no index boundary check since parser guarantees balanced expression */
			op1 = val_stack[--stack_index];
			switch(token_get_function(cur_token)) {
				case FnNeg:
					op1 = -op1; break;
				case FnSin:
					op1 = sin(op1); break;
				case FnCos:
					op1 = cos(op1); break;
				case FnSqrt:
					op1 = sqrt(op1); break;
				case FnLn:
					op1 = log(op1); break;
				default:
					fail("evaluate_expression: invalid function");
					break;
			}
			val_stack[stack_index++] = op1;
		} else if (token_isoperator(cur_token)) {
			/* no index boundary check since parser guarantees balanced expression */
			op1 = val_stack[--stack_index];
			op2 = val_stack[--stack_index];
			switch(token_get_operator(cur_token)) {
				case OpAdd:
					op1 = op2 + op1; break;
				case OpSub:
					op1 = op2 - op1; break;
				case OpMul:
					op1 = op2 * op1; break;
				case OpDiv:
					op1 = op2 / op1; break;
				case OpPow:
					op1 = pow(op2, op1); break;
				default:
					fail("evaluate_expression: invalid operator");
					break;
			}
			val_stack[stack_index++] = op1;
		} else {
			fail("evaluate_expression: unknown token type");
		}
	}

	if (stack_index != 1) {
		fail("evaluate_expression: expression unbalanced");
	}

	return val_stack[0];
}

/****************************/
/* END evaluator function   */
/****************************/

int main(int argc, char **argv) {

	char *line_buffer=NULL; size_t line_length=0;
	long int index, num_intervals, num_experiments; /* long int is required but size_t is ok, i guess ? */
	fp_t lower_limit=DBL_MAX, upper_limit=-DBL_MAX;
	long int output_histogram[COUNT_PDF_MAX]={0};

	variable_t variables[NUM_VARIABLES] = {0};
	token_queue_t postfix_expression;

	UNUSED(argc);
	UNUSED(argv);

	/* initialize variables and utilities */
	queue_init(&postfix_expression);
	util_init();

	/* get infix expression to line_buffer */
	if (util_getline(&line_buffer, &line_length, stdin) < 0) {
		fail("expression read error");
	}

	/* parse the infix expression to postfix expression */
	parse_expression(line_buffer, &postfix_expression);

	/* get interval and experiment count */
	if (scanf("%ld %ld\n", &num_intervals, &num_experiments) != 2) {
		fail("num_intervals num_experiments scanf error");
	}

	/* get variables */
	for (;;)
	{
		variable_t tmp; int rv;
		if ((rv = util_getline(&line_buffer, &line_length, stdin)) < 0) {
			fail("getline returned error");
		} else if (rv == 0) {
			break;
		} else {
			tmp = variable_parse_str(line_buffer, num_intervals);
			variables[util_var_index(tmp.name)] = tmp;
		}
	}

	free(line_buffer);
	line_buffer = NULL;

	#ifdef __DEBUG__
	/* check whether all used variables in expr are defined */
	{
		token_t *ptoken;
		for (ptoken = postfix_expression.data; 
			ptoken < postfix_expression.data + postfix_expression.tail; 
			++ptoken) {
			if (token_isvariable(*ptoken))
				if (! variables[util_var_index(ptoken->value.variable)].name)
					fail("variable used in expr was not present in given input");
		}
	}
	#endif

	/* two-pass approach, first pass to determine bounds
	   second pass to calculate actual histogram */
	for (index = 0; index < num_experiments; ++index) {
		fp_t result, sampled_variables[NUM_VARIABLES];
		/* sample all variables (non existings set to 0.0 */
		util_random_sample_all(sampled_variables, variables, NUM_VARIABLES);

		/* evaluate the expression with sampled variables */
		result = evaluate_expression(&postfix_expression, sampled_variables);

		if (result < lower_limit)
			lower_limit = result;
		if (result > upper_limit)
			upper_limit = result;
	}

	for (index = 0; index < num_experiments; ++index) {
		fp_t result, sampled_variables[NUM_VARIABLES];
		size_t bin_index;
		/* sample all variables (non existings set to 0.0 */
		util_random_sample_all(sampled_variables, variables, NUM_VARIABLES);

		/* evaluate the expression with sampled variables */
		result = evaluate_expression(&postfix_expression, sampled_variables);

		/* find the bin index in histogram given the limits */
		bin_index = util_get_bin(lower_limit, upper_limit, num_intervals, result);

		output_histogram[bin_index]++;
	}

	printf("%f %f", lower_limit, upper_limit);

	for (index = 0; index < num_intervals; ++index) {
		printf(" %f", (fp_t) output_histogram[index] / num_experiments);
	}
	printf("\n");

	return 0;
}